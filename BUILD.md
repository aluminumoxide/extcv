# These are the steps I needed to build with Visual Studio 2019

## Install NASM
Install NASM and modify PATH so nasm.exe can be found. Source for NASM:
    https://www.nasm.us/pub/nasm/releasebuilds/2.15/
You may need to close and reopen Visual Studio after modifying PATH.

## Install Cryptoki headers
Find the cryptoki header files. Discussion here:
    https://crypto.stackexchange.com/questions/26601/source-for-pkcs11-header-files
File can be found:
    https://www.oasis-open.org/committees/document.php?document_id=55657&wg_abbrev=pkcs11
	https://www.oasis-open.org/committees/document.php?document_id=55656&wg_abbrev=pkcs11
	https://www.oasis-open.org/committees/document.php?document_id=55655&wg_abbrev=pkcs11
You may choose to install the files to a local directory and modify the include path to
reference that location, or add that include path to the project properties.

## Install MFC
Find the cryptoki header files. Discussion here:
Open the Visual Studio Installer and Install the MFC component, as well as the component category
for C++ desktop development.

## Build and misc
Open the project in Visual Studio.

If you see the following error:
    "Error HRESULT E_FAIL has been returned from a call to a COM component".
Close Visual Studio, remove the project .suo file,
    .vs/extcv/v16/.suo
and reopen Visual Studio.

Build the project. The built executable will be in:
    ExpandVolume/Debug/extcv.exe
Or:
    ExpandVolume/Release/extcv.exe
